package it.polito.discarding_Strategy;

import java.util.ArrayList;


import it.polito.sol.*;

public interface PacketDiscarding {
	
	public void discard(Vehicle v);
	public ArrayList<String> getPackets_logged();
	public void setPackets_logged(ArrayList<String> packets_logged);
	public String getNameStrategy();
	public void setNameStrategy(String name);
	public Integer getNumTotPacket();
	public void setNumTotPacket(Integer numTotPacket) ;
	public Integer getNumTotDiscarded() ;
	public Integer getNumTotLogged() ;
	public void svuota();
}//fine classe
