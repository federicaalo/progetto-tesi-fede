package it.polito.discarding_Strategy;
import it.polito.sol.Vehicle;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Random;


public class distanceBased implements PacketDiscarding{

	private String nameStrategy;
	private ArrayList<String> packets_logged; 
	private ArrayList<String> packets_discarded; 
	private Float[] range;
	private Integer[] range_ratio;
	private Integer numTotDiscarded;
	private Integer numTotLogged;
	private Integer numTotPacket; //numero pacchetti totali ricevuti

	public distanceBased(String name, Float[] range, Integer[] range_ratio){
		this.setNameStrategy(name);
		this.range=range;
		this.range_ratio=range_ratio;

	}

	public void discard(Vehicle v) {
	
		packets_logged=new ArrayList<String>();
		packets_discarded=new ArrayList<String>();
		if(this.numTotPacket==null){this.numTotPacket=new Integer(0);}

		if(v.getPackets_received()==null){
			
			return;
		}
		else{
			//if(this.numTotPacket==null){this.numTotPacket=new Integer(0);}
			this.numTotPacket=this.numTotPacket+v.getNumPacketsR(); 
			Random random = new Random();

			for(Entry<Float,ArrayList<Vehicle>> entry: v.getVehicleIndistance().entrySet()){
				Integer ratio = null;
				for(Integer i=0;i<this.range.length;i++){
					if(entry.getKey()<=range[i]){ //se distanza è minore della grandezza della fascia allora sono in quella fascia
						
						ratio=range_ratio[i];// ho trovato la fascia quindi salvo la percentuale per quella fascia
						break;
					}
				}
				for(Vehicle ve:entry.getValue()){
					if(random.nextDouble()<(double)ratio/100){ //se il numero che genero è < della percentuale allora prendo il pacchetto
						//la dipendenza dai pacchetti ricevuti non c'è perchè decido a runtime se loggarli o meno
						if(ve.getPacketSent()!=null){
							this.packets_logged.add(ve.getPacketSent());
							v.getPacketLogged().add(ve.getPacketSent());
							//FIXME
							if (this.numTotLogged==null){ this.numTotLogged=new Integer(0);}
							this.numTotLogged++;
							v.setNumLoggedPacket(v.getNumLoggedPacket()+1);
						}
					}

					else{
						if(ve.getPacketSent()!=null){
							if (this.numTotDiscarded==null){ this.numTotDiscarded=new Integer(0);}
							this.numTotDiscarded++;
							this.packets_discarded.add(ve.getPacketSent());
							v.getPacketDiscarded().add(ve.getPacketSent());
							v.setNumDiscardedPacket(v.getNumDiscardedPacket()+1);
						}
					}
					if (this.numTotDiscarded==null){ this.numTotDiscarded=new Integer(0);}					if (this.numTotLogged==null){ this.numTotLogged=new Integer(0);}
					if (this.numTotLogged==null){ this.numTotLogged=new Integer(0);}

				}
			}

		}
		

	}

	@Override
	public ArrayList<String> getPackets_logged() {
		return this.packets_logged;
	}

	@Override
	public void setPackets_logged(ArrayList<String> packets_logged) {
		this.packets_logged=packets_logged;

	}
	/**
	 * @return the nameStrategy
	 */
	public String getNameStrategy() {
		return this.nameStrategy;
	}
	/**
	 * @param nameStrategy the nameStrategy to set
	 */
	public void setNameStrategy(String nameStrategy) {
		this.nameStrategy = nameStrategy;
	}
	/**
	 * @return the numTotPacket
	 */
	public Integer getNumTotPacket() {
		if(this.numTotPacket==null)
			this.numTotPacket=new Integer(0);
		return this.numTotPacket;
	}

	/**
	 * @param numTotPacket the numTotPacket to set
	 */
	public void setNumTotPacket(Integer numTotPacket) {
		this.numTotPacket = numTotPacket;
	}
	public Integer getNumTotDiscarded() {
		if(this.numTotDiscarded==null)
			this.numTotDiscarded=new Integer(0);
		return this.numTotDiscarded;
	}

	public Integer getNumTotLogged() {
		if(this.numTotLogged==null)
			this.numTotLogged=new Integer(0);
		return this.numTotLogged;
	}
	/**
	 * @return the range_ratio
	 */
	public Integer[] getRange_ratio() {
		return this.range_ratio;
	}

	/**
	 * @param range_ratio the range_ratio to set
	 */
	public void setRange_ratio(Integer[] range_ratio) {
		this.range_ratio = range_ratio;
	}
	/**
	 * @return the range
	 */
	public Float[] getRange() {
		return this.range;
	}
	/**
	 * @param range the range to set
	 */
	public void setRange(Float[] range) {
		this.range = range;
	}

	/**
	 * svuota
	 */
	public void svuota(){
		this.nameStrategy=null;
		this.numTotDiscarded=null;
		this.numTotLogged=null;
		this.numTotPacket=null;

		if(this.packets_logged!=null&& !this.packets_logged.isEmpty())
			this.packets_logged.clear();
		
		if(this.packets_discarded!=null&& !this.packets_discarded.isEmpty())
			this.packets_discarded.clear();
		
	}
}
