package it.polito.discarding_Strategy;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.Random;


import it.polito.sol.*;

public class random implements PacketDiscarding{
	
	private String nameStrategy;
	//private int num; //num random
	private Integer numTotDiscarded;
	private Integer numTotLogged;
	private Integer numTotPacket;
	private ArrayList<String> packets_logged; //float= tempo generale non intervallini
	private Integer ratio; //probabilità di prendere il singolo pacchetto
	
	
	public random(String name, Integer ratio){
		this.setNameStrategy(name);
		this.ratio=ratio;
	}
	
	/**
	 * @param v=veicolo
	 * strategia che scarta un numero random di pacchetti 
	 * dalla lista dei pacchetti ricevuti
	 * numero random preso tra 0 e la metà dei pacchetti ricevuti
	 */
	public void discard(Vehicle v){	
	
		packets_logged=new ArrayList<String>();
		if(this.numTotPacket==null){this.numTotPacket=new Integer(0);}
		if(v.getNumPacketsR()==0){
			
			return;
		}
		else{
			this.numTotPacket+=v.getNumPacketsR();
			Random random = new Random();
			
			for(Entry<Float,ArrayList<String>>  entry:v.getPackets_received().entrySet()){
				for(String p:entry.getValue()){
					if(random.nextDouble()<(double)ratio/100){ //se il numero che genero è < della percentuale allora prendo il pacchetto
						//la dipendenza dai pacchetti ricevuti non c'è perchè decido a runtime se loggarli o meno
						this.packets_logged.add(p);
						v.getPacketLogged().add(p);
						//FIXME
						if (this.numTotLogged==null){ this.numTotLogged=new Integer(0);}
						this.numTotLogged++;
						v.setNumLoggedPacket(v.getNumLoggedPacket()+1);
					}
					else{
						if (this.numTotDiscarded==null){ this.numTotDiscarded=new Integer(0);}
						this.numTotDiscarded++;
						v.setNumDiscardedPacket(v.getNumDiscardedPacket()+1);
					}
					if (this.numTotLogged==null){ this.numTotLogged=new Integer(0);}
					if (this.numTotDiscarded==null){ this.numTotDiscarded=new Integer(0);}

				}
			}
			//System.out.println("num_pacchetti_ricevuti: "+v.getNumPacketsR()+" num pacchetti loggati  "+v.getNumLoggedPacket() +" numero pacchetti scartati "+v.getNumDiscardedPacket());

//			v.setPacketLogged(this.packets_logged);
		}
		
	}

	/**
	 * @return the packets_logged
	 */
	public ArrayList<String> getPackets_logged() {
		return this.packets_logged;
	}

	/**
	 * @param packets_logged the packets_logged to set
	 */
	public void setPackets_logged(ArrayList<String> packets_logged) {
		this.packets_logged = packets_logged;
	}

	/**
	 * @return the nameStrategy
	 */
	public String getNameStrategy() {
		return this.nameStrategy;
	}

	/**
	 * @param nameStrategy the nameStrategy to set
	 */
	public void setNameStrategy(String nameStrategy) {
		this.nameStrategy = nameStrategy;
	}

	/**
	 * @return the numTotPacket
	 */
	public Integer getNumTotPacket() {
		return this.numTotPacket;
	}

	/**
	 * @param numTotPacket the numTotPacket to set
	 */
	public void setNumTotPacket(Integer numTotPacket) {
		this.numTotPacket = numTotPacket;
	}
	public Integer getNumTotDiscarded() {
		return this.numTotDiscarded;
	}

	public Integer getNumTotLogged() {
		return this.numTotLogged;
	}
	public void svuota(){
		this.nameStrategy=null;
		this.numTotDiscarded=null;
		this.numTotLogged=null;
		this.numTotPacket=null;
		this.ratio=null;
		
		if(this.packets_logged!=null&& !this.packets_logged.isEmpty())
			this.packets_logged.clear();
	}
}
