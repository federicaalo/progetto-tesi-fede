package it.polito.sol;

public class Constant {

	private int numint= 10;
	private float dmin=0;
	private float dmax=100;// raggio massimo del cerchio
	private int ratio=50;
	private Float[] range;// dimensione delle fasce
	private Integer[] range_ratio={100,80,60,40,20}; //percentuali con cui prendo i pacchetti(fasce)
	private Integer soglia=new Integer(3);
	
	
	private Integer[] pesi= {11,11,30,11,11,20,3,3}; //la somma dei pesi deve essere 100
	/**
	 * pesi[0] loggati da un veicolo
	 * pesi[1] scartati da un veicolo
	 * pesi[2] consecutivi non loggati
	 * pesi[3] loggati in un tempo
	 * pesi[4] scartati in un tempo
	 * pesi[5] volte che uno stesso pacch viene loggato
	 * pesi[6] veicoli in un tempo
	 * pesi[7] veicoli che hanno ricevuto in un tempo
	 */
	
	public Constant(){
		this.range=new Float[this.range_ratio.length];
		for (Integer i=1;i<=this.range_ratio.length;i++){
			this.range[i-1]=i*(this.dmax/this.range_ratio.length);
		}
	}
	
	/**
	 * @return the numint
	 */
	public int getNumint() {
		return this.numint;
	}
	/**
	 * @param numint the numint to set
	 */
	public void setNumint(int numint) {
		this.numint = numint;
	}

	/**
	 * @return the dmin
	 */
	public float getDmin() {
		return this.dmin;
	}

	/**
	 * @param dmin the dmin to set
	 */
	public void setDmin(float dmin) {
		this.dmin = dmin;
	}

	/**
	 * @return the dmax
	 */
	public float getDmax() {
		return this.dmax;
	}

	/**
	 * @param dmax the dmax to set
	 */
	public void setDmax(float dmax) {
		this.dmax = dmax;
	}

	/**
	 * @return the ratio
	 */
	public int getRatio() {
		return this.ratio;
	}

	/**
	 * @param ratio the ratio to set
	 */
	public void setRatio(int ratio) {
		this.ratio = ratio;
	}

	/**
	 * @return the range_ratio
	 */
	public Integer[] getRange_ratio() {
		return this.range_ratio;
	}

	/**
	 * @param range_ratio the range_ratio to set
	 */
	public void setRange_ratio(Integer[] range_ratio) {
		this.range_ratio = range_ratio;
	}
	
	/**
	 * @return the range
	 */
	public Float[] getRange(){
		return this.range;
	}

	/**
	 * @return the soglia
	 */
	public Integer getSoglia() {
		return this.soglia;
	}

	/**
	 * @param soglia the soglia to set
	 */
	public void setSoglia(Integer soglia) {
		this.soglia = soglia;
	}

	/**
	 * @return the pesi
	 */
	public Integer[] getPesi() {
		return this.pesi;
	}

	/**
	 * @param pesi the pesi to set
	 */
	public void setPesi(Integer[] pesi) {
		this.pesi = pesi;
	}
	
	public void printMemory(){
		int mb = 1024*1024;
		 Runtime runtime = Runtime.getRuntime();
		 System.out.println("Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb+" MB");
	}
}
