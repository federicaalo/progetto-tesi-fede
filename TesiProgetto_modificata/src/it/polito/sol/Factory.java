package it.polito.sol;

import genVtypeprobe.VtypeprobesType;
import it.polito.discarding_Strategy.PacketDiscarding;
import it.polito.discarding_Strategy.distanceBased;
import it.polito.discarding_Strategy.random;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.xml.bind.JAXBElement;

public class Factory {
	
	private static Object[] old_stat;

	public static void main(String[] args) throws MyException{
		double startTime = System.currentTimeMillis();
		System.out.println("main........ ");
			
		 Map<String,String>keymapHashMap =new HashMap<String,String>();
		 Statistics stat=null; // spostando cosi dovresti allocare meno memoria
		try {
			Boolean piccolo=true; //true = simulazione file piccolo(50s) false= simulazione file grande(1h)
			int max;
			if(piccolo){
				max=3;
			}else{
				max=42;
			}
			Constant c= new Constant();
			System.out.println("dimensione heap:"+Runtime.getRuntime().freeMemory()/(1024*1024)+" MB\n ");
			HashMap<Float, ArrayList<Vehicle>>	time_map = null;
			HashMap<String, TreeMap<Float, Vehicle>> all_vehicles = null;
			PacketDiscarding discarding=null;
			int i;
				for(i=1;i<=max;i++){
					time_map = new HashMap<Float, ArrayList<Vehicle>>();
					all_vehicles = new HashMap<String, TreeMap<Float, Vehicle>>();
				
					/**============= Parsing of input file ====================================**/
					System.out.println(" Start parsing file: "+i+"dimensione heap:"+Runtime.getRuntime().freeMemory()/(1024*1024)+" MB\n ");
					
					JAXBElement<VtypeprobesType> elem_v = null;
					ParseXML parser=new ParseXML(c,i,piccolo,time_map,all_vehicles,keymapHashMap);
					
					elem_v=null;
					System.out.println(" end parsing file: "+i);
					c.printMemory();
							
					parser=null;
					System.gc();
					Runtime.getRuntime().gc();
					Runtime.getRuntime().gc();
					System.out.println(" gc after parsing file: "+i);
					c.printMemory();
					/**============= sending and receiving packets simulation =================**/
					
					Simulation2 sim=new Simulation2(time_map,all_vehicles,c); //passo mappe
					
					System.out.println("New Simulation dimensione heap:");
					c.printMemory();

					sim.simulationSent(); //simulazione invio-ricezione		
					System.out.println("Sim Sent dimensione heap:");
					c.printMemory();

					
					/**============= discarding strategy =======================================**/
					/**random -->*/ 	//discarding=new random("Random",this.c.getRatio());
					/**distance based -->*/	discarding=new distanceBased("Distance based",c.getRange(),c.getRange_ratio());
					System.out.println("New Discarding dimensione heap:");
					c.printMemory();

					List<Float> list=new ArrayList<Float>(); //per scorrere i tempi in modo ordinato
					list.addAll(time_map.keySet());
					Collections.sort(list);
					stat=new Statistics(c, old_stat);
					System.out.println("NEW statistic dimensione heap:");
					c.printMemory();
					stat.setTotTime(list.size());
					stat.setAllVehicles(all_vehicles);
					stat.setTecnicaScarto(discarding.getNameStrategy());
					
					int countVehicles=0;
					for(Float iter:list){ //tempi
						int countLogged=0;
						int countDiscarded=0;
						for(Vehicle v:time_map.get(iter)){ //veicoli
							countVehicles++;
							discarding.discard(v); //scarto
							countLogged=countLogged+v.getNumLoggedPacket();
							countDiscarded=countDiscarded+v.getNumDiscardedPacket();
						}//fine veicoli
					
						/**============= statistics =======================================**/
						stat.Packets(iter, countLogged, countDiscarded,time_map.get(iter));
						stat.NumLog_SamePacket(time_map.get(iter), iter);
						stat.VehicleThatReceived(time_map.get(iter), iter);
						old_stat=stat.getResult();
					}//fine tempi
					System.out.println("count ogg veicoli...............:"+countVehicles);
					stat.setTotOggVehicles(countVehicles);
					stat.Logged_from_a_Vehicle(); //salva solo info in una mappa, non serve per calcolo medie
					stat.ConsecutiveDiscarded(); //pacchetti scartati consecutivamente
					stat.TotLogged_discarded(discarding.getNumTotLogged(), discarding.getNumTotDiscarded(),discarding.getNumTotPacket());
					System.out.println(" PRima free memoria allocata:  ");
					c.printMemory();
					
					/**================== free =================**/
					old_stat=stat.getResult();

					sim.svuota();
					discarding.svuota();
				//	svuota(time_map,all_vehicles);
				time_map=null; 
				all_vehicles=null;
					System.gc();//suggerisce esecuzione garbage collector
				}
				/**============= result statistics and performance metric =================**/
				stat.Stampa();
				stat.Metrica();
				System.out.println("\n 		Risultato metrica.....: " +stat.getPerformance());
				System.out.println(" 		peggiore.....: " +stat.getPeggiore());
				System.out.println(" 		migliore.....: " +stat.getMigliore());
				System.out.println(" 		medio.....: " +stat.getMedio()); //non è eccellente
				//stat.stampaPacchettiLoggati();
				
				double endTime = System.currentTimeMillis(); 
				System.out.println(" DOPO stampa statistiche memoria allocata:  ");
				c.printMemory();
				
				
				System.out.println("\nTempo di esecuzione programma= " + (endTime - startTime)/1000+"s");
				
			
			
		}
		catch (Exception e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(-1);
			
		}
	}
	
	public static void svuota(HashMap<Float, ArrayList<Vehicle>>	time_map,HashMap<String, TreeMap<Float, Vehicle>> all_vehicles){
		
			if(time_map.size()!=0){
				for(Entry<Float,ArrayList<Vehicle>> e:time_map.entrySet()){
					for(Vehicle v:e.getValue()){
						if(v!=null){
							v.svuota();
							v=null;
						}
					}
					e.getValue().clear();
				}
				time_map.clear();
			}
			
			if(all_vehicles!=null && !all_vehicles.isEmpty()){
				for(Entry<String,TreeMap<Float,Vehicle>> Veicolo:all_vehicles.entrySet()){
					for(Entry<Float,Vehicle> ogg:Veicolo.getValue().entrySet()){
						if(ogg.getValue()!=null){
							ogg.getValue().svuota();
						}
						Veicolo.getValue().clear();
					}
				}
				all_vehicles.clear();
			}
		}

}
