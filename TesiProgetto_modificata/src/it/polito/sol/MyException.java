package it.polito.sol;

public class MyException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public MyException(String msg){
		super(msg);
	}
	
	public MyException(){
		super();
	}
	public MyException(Exception e, String msg){
		super(msg,e);
	}


}
