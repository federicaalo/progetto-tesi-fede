package it.polito.sol;

public class Packet {

	private String id;
	private Float lat;
	private Float lon;
	private Vehicle v;
	private Float creationtime; // tempo di invio , tempo di ricezione (comprende intervallini)
	
	public Packet( Vehicle v,Float lat,Float lon,Float creationtime){
		//this.id=v.getId()+"-"+creationtime.toString();
		this.lat=lat;
		this.lon=lon;
		this.v=v;
		this.setCreationtime(creationtime);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the lat
	 */
	public Float getLat() {
		return this.lat;
	}

	/**
	 * @param lat the lat to set
	 */
	public void setLat(Float lat) {
		this.lat = lat;
	}

	/**
	 * @return the lon
	 */
	public Float getLon() {
		return this.lon;
	}

	/**
	 * @param lon the lon to set
	 */
	public void setLon(Float lon) {
		this.lon = lon;
	}

	/**
	 * @return the v
	 */
	public Vehicle getV() {
		return this.v;
	}

	/**
	 * @param v the v to set
	 */
	public void setV(Vehicle v) {
		this.v = v;
	}

	/**
	 * @return the creationtime
	 */
	public Float getCreationtime() {
		return this.creationtime;
	}

	/**
	 * @param creationtime the creationtime to set
	 */
	public void setCreationtime(Float creationtime) {
		this.creationtime = creationtime;
	}
	
	/*public void svuota(){
		
		this.lat=null;
		this.lon=null;
		this.creationtime=null;
	}*/
}
