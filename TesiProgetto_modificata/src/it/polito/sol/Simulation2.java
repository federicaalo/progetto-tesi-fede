package it.polito.sol;

import java.util.*;
import java.util.Map.Entry;

public class Simulation2 {

	private Constant c;
	private HashMap<Float,ArrayList<Vehicle>> time_map; //
	private HashMap<String,TreeMap<Float,Vehicle>> all_vehicles; 
	private HashMap<Integer,ArrayList<Vehicle>> Interval_map; //mappa di lista di veicoli per ogni intervallo
	private HashMap<String,Integer> intervalls; //FIXME mappa delle corrispondenze tra id veicolo e suo intervallo di invio

	//private HashMap<Float, HashMap<Integer, ArrayList<Vehicle>>> result_sent = new HashMap<Float, HashMap<Integer, ArrayList<Vehicle>>>();
	// mappa che contiene la mappa degli intervalli per ogni tempo,
	// ad ogni veicolo viene assegnato un intervallo per sempre la prima volta che invia un pacchetto,
	// ogni intervallo, quindi, ha una lista di veicoli

	/**
	 * costruttore
	 * @param time_map
	 * @param all_vehicles
	 */
	public Simulation2( HashMap<Float,ArrayList<Vehicle>> time_map,HashMap<String,TreeMap<Float,Vehicle>> all_vehicles, Constant c){
		this.time_map=time_map;
		this.all_vehicles=all_vehicles;
		this.c=c;
	}
	
	public void simulationSent() throws MyException{
		Integer interval;
		List<Float> list=new ArrayList<Float>(); //per scorrere i tempi in modo ordinato 
		list.addAll(time_map.keySet());
		Collections.sort(list);
		
		for(Float iter:list){ //loop tempi
			
			/**============= sending =======================================**/
			Interval_map=new HashMap<Integer,ArrayList<Vehicle>>();
			Integer tmp=this.c.getNumint();//performance^^
			for(Integer w=0;w<tmp;w++){
				ArrayList<Vehicle> l=new ArrayList<Vehicle>(); //istanzio le liste vuote per ogni intervallo (numint=numero intervalli)
				Interval_map.put(w,l);
			}	
		
			ArrayList<Vehicle> vehicle_list=time_map.get(iter);
			for(Vehicle v:vehicle_list){ //scorro lista veicoli
				interval=null;
				ArrayList<Vehicle> listv;
				
				if(all_vehicles.containsKey(v.getId())){// per sicurezza 
					
					for (Entry<Float,Vehicle> entry1: all_vehicles.get(v.getId()).entrySet() ){ 
							if((interval=entry1.getValue().getInterval())!=null){
								break;
							}
					}
					
					if(interval!=null){// se campo intervallo esiste vuol dire che ho già inviato prima
						listv=Interval_map.get(interval);
						listv.add(v); //aggiungo veicolo alla lista	
						Interval_map.put(interval, listv);
					}
					else{ //se campo intervallo e'� nullo, non ho mai inviato, devo assegnare intervallo
						Random rand=new Random();
						interval=rand.nextInt(this.c.getNumint());
						v.setInterval(interval,iter);				
						listv=Interval_map.get(interval);
						listv.add(v); //aggiungo veicolo alla lista
						Interval_map.put(interval,listv);
					}
					v.convert_time(iter, interval);
				}
				
				//Packet packet = new Packet(v,v.getLat(),v.getLon(),v.getConvertedTime());
				
				//v.setPackets_sent(packet); //setto la lista dei pacchetti inviati, aggiungendo il nuovo pacchetto inviato				
			
				/**============= receiving =======================================**/
				simulationReceived(v.getV100m(),v.getId(),iter,interval); //passo lista veicoli a 100m
				
			}// fine for veicoli
		}//fine for dei tempi
		System.out.println("End simulation........");
		
		/**************  STAMPE  *****************/
	//	Stampa_PacchettiInviati(all_vehicles);
	//	Stampa_PacchettiRicevuti(all_vehicles);
	//	Stampa_distanze(all_vehicles);
	//	Stampa_PacchettiLoggati(all_vehicles);
	//	Stampa_veicoli100m(all_vehicles);
		//this.stampaAllVehicle();
		//stat.Stampa();
	}
	
	/**
	 * per ogni veicolo che invia un pacchetto inserisco, nella lista pacchetti ricevuti dei veicoli 
	 * a 100m, quel pacchetto
	 * vlist= passo la lista dei veicoli a 100m dal veicolo, sono i veicoli che ricevono il pacchetto
	 * */
	public void simulationReceived(ArrayList<Vehicle> vlist,String p,Float time,Integer i){
		//System.out.println(System.currentTimeMillis()+" start SimulationReceived ");
		Float t;
		t=convert_time(time,i);
		for(Vehicle v:vlist){
			v.setPackets_received(t, p);
		}
		//System.out.println(System.currentTimeMillis()+"  end SimulationReceived");
	}
	
	/**
	 * svuoto le mappe di questa classe e gli oggetti che contengono
	 */
	public void svuota(){
		if(this.Interval_map!=null && !this.Interval_map.isEmpty()){
			for(Entry<Integer,ArrayList<Vehicle>> e:this.Interval_map.entrySet()){
				for(Vehicle v:e.getValue()){
					if(v!=null){
					v.svuota();
					v=null;}
				}
				e.getValue().clear();
				e=null;
			}
			this.Interval_map.clear();
		}
	}

	public Float convert_time(Float time,Integer i){
		Float t=new Float((time+((float)i/100)));
		t=(float)Math.round(t*100);
		t=t/100;
		return t;
	}
	public HashMap<Float,ArrayList<Vehicle>> getTimeMap(){
		return this.time_map;
	}
	public HashMap<String,TreeMap<Float,Vehicle>> getAllVehicles(){
		return this.all_vehicles;
	}
	public HashMap<Integer,ArrayList<Vehicle>> getIntervalMap(){
		return this.Interval_map;
	}
	/**
	 * @return the intervalls
	 */
	public HashMap<String,Integer> getIntervalls() {
		if(this.intervalls==null)
			this.intervalls=new HashMap<String,Integer>();
		return this.intervalls;
	}

	/**
	 * @param intervalls the intervalls to set
	 */
	public void setIntervalls(HashMap<String,Integer> intervalls) {
	
		this.intervalls = intervalls;
	}

	
	
	
	
	
	
	
	
	
/***==================================================== STAMPE =====================================================***/
	public void stampaAllVehicle(){
		for(Entry<String,TreeMap<Float,Vehicle>> veicolo:this.getAllVehicles().entrySet()){
			System.out.println("\nveicolo....."+veicolo.getKey());
			for(Entry<Float,Vehicle> tempo:veicolo.getValue().entrySet()){
				System.out.println("tempo....."+tempo.getKey()+"   oggetto veicolo....."+tempo.getValue().getId());
			}
		}
	}
	
	
	public void StampaVeicoli(HashMap<Float,HashMap<Integer, ArrayList<Vehicle>>> map, Float iter){
		// stampa veicoli per ogni intervallo di tempo
		System.out.println(" \n");
		System.out.println("\nTEMPO......."+iter);
		Float time, timesent;
		Integer intervallo;
		List<Float> list=new ArrayList<Float>(); //per scorrere i tempi in modo ordinato
		list.addAll(map.keySet());
		Collections.sort(list);
		time=iter;
		for(Entry<Integer, ArrayList<Vehicle>> entry2:map.get(iter).entrySet()){
			intervallo =entry2.getKey();
			//conv=(float)intervallo/100;
			timesent=convert_time(time,intervallo);
			//timesent=new Float((time+((float)intervallo/100)));
			for(Vehicle v:entry2.getValue()){
				if(v.getNumPacketsR()!=0){
					System.out.println("        interval...."+intervallo+"        veicolo...." +v.getId()+"  timesent= "+timesent+"      numero pacchetti ricevuti: "+v.getNumPacketsR());
				}
			}
		}
	}
	
/*	public void Stampa_PacchettiInviati(HashMap<String,TreeMap<Float,Vehicle>> map){
		//per ogni veicolo, per ogni tempo viene stampato il pacchetto inviato (in quel tempo)
		System.out.println("\nPACCHETTI INVIATI");
		for(Entry<String,TreeMap<Float,Vehicle>> entry:map.entrySet()){
			System.out.println(" ID veicolo....."+entry.getKey());
			//TreeMap<Float,Object[]> packets_sent=v.getPackets_sent();
			for(Entry<Float,Vehicle>  entry2:entry.getValue().entrySet()){
				Vehicle v=entry2.getValue();
				for(Entry<Float,Object[]> entry3:v.getPackets_sent().entrySet()){
					Object[] obj=entry3.getValue();
					Packet p=(Packet) obj[3];
					System.out.println("      time...."+entry3.getKey()+"    pacchetto-id v...."+p.getV().getId());
				}
			}
		}
	}*/
	
	public void Stampa_PacchettiRicevuti(HashMap<String,TreeMap<Float,Vehicle>> map){
	
		System.out.println("\nPACCHETTI RICEVUTI");
		for(Entry<String,TreeMap<Float,Vehicle>> entry:map.entrySet()){
			System.out.println("\nveicolo....."+entry.getKey());
		//	TreeMap<Float, ArrayList<Packet>> packets_rec=v.getPackets_received();
			for(Entry<Float,Vehicle>  entry2:entry.getValue().entrySet()){
				Vehicle v=entry2.getValue();
				if(v.getNumPacketsR()!=0){
					System.out.println("\n     time....."+entry2.getKey());
					for(Entry<Float, ArrayList<String>>  entry3:v.getPackets_received().entrySet()){
						Float tempo=entry3.getKey();
						//size=sono i pacchetti ricevuti negli intervallini
						System.out.println("               interv ="+tempo+"   size= "+entry3.getValue().size()+"  num pacchetti rievuti= "+v.getNumPacketsR());
						//System.out.println(" size.....................="+plist.size());
						for(String p:entry3.getValue()){
							System.out.println("                      pacchetto-id veicolo..."+p);
						}
					}
				}
			}
		}
	}
	
	public void Stampa_PacchettiLoggati(HashMap<String,TreeMap<Float,Vehicle>> map){
		
		System.out.println("\nPACCHETTI LOGGATI    ");
		for(Entry<String,TreeMap<Float,Vehicle>> entry:map.entrySet()){
			System.out.println("\nveicolo....."+entry.getKey());
		//	TreeMap<Float, ArrayList<Packet>> packets_rec=v.getPackets_received();
			for(Entry<Float,Vehicle>  entry2:entry.getValue().entrySet()){
				Integer cnt= new Integer(0);
				Vehicle v=entry2.getValue();
				if(v.getNumPacketsR()!=null){
					System.out.println("\n     time intervallino....."+entry2.getKey());
					for(String p:v.getPacketLogged()){
						//System.out.println("    p= "+p);				
						System.out.println("      pacchetto-id veicolo..."+p);
						cnt++;
					}
				}
				System.out.println("pacchetti loggati (num altra funzione): "+v.getNumLoggedPacket()+" num pacchetti loggati contati: "+cnt);
			}
		}
	}
	
	
	public void Stampa_veicoli100m(HashMap<String,TreeMap<Float,Vehicle>> vmap){
		System.out.println("\nveicoli a 100m");
		for(Entry<String,TreeMap<Float,Vehicle>> entry:vmap.entrySet()){
			System.out.println("\nveicolo....."+entry.getKey());
			int count=0;
			for(Entry<Float,Vehicle>  entry2:entry.getValue().entrySet()){
				
				Vehicle v=entry2.getValue();
				if(v.getV100m().size()!=0)
					System.out.println("   NUM veicoli: "+v.getV100m().size());
				for(Vehicle ve:v.getV100m()){
					count++;
					System.out.println("     veicolo: "+ve.getId());
				}
			}
			if(count!=0)
				System.out.println("     dim: "+count);
		}
	}
	
	public void Stampa_prova(HashMap<String,TreeMap<Float,Vehicle>> map){
		
		System.out.println("\ndistanza veicoli.......");
		for(Entry<String,TreeMap<Float,Vehicle>> entry:map.entrySet()){
			for(Entry<Float,Vehicle>  entry2:entry.getValue().entrySet()){
				Vehicle v=entry2.getValue();
			
				/*if(v.getId().equals("41525_41525_361")){
					HashMap<Double,ArrayList<Vehicle>> dist=v.getDistances();
					System.out.println("   veicolo 41525_41525_361.......");
					for(Entry<Double, ArrayList<Vehicle>>  entry2:dist.entrySet()){
						System.out.println("     distanza:......."+entry2.getKey());
					}
				}*/
				if(v.getId().equals("37998_37998_358")){
					ArrayList<Vehicle> list=v.getV100m();
					//HashMap<Float,ArrayList<Vehicle>> dist=v.getDistances();
					System.out.println("   veicolo 37998_37998_358......."+entry2.getValue().getV100m());
					for(Vehicle entry3:list){
						System.out.println("     v:......."+entry3.getId());
					}
				}
				/*if(v.getId().equals("41525_41525_361")){
					System.out.println("veicolo 41525_41525_361.......");
					ArrayList<Vehicle> list=v.getVehicleIndistance(0.0, 100.0);
					for(Vehicle vehicle:list){
						System.out.println("   veicolo..."+v.getId());
					}
				}*/
			}
		}
	}

	
	
}//fine classe
