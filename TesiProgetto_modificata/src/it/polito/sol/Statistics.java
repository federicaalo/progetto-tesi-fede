package it.polito.sol;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;



public class Statistics {
	private Constant c;
	private String tecnica_scarto; // nome della tecnica di scarto
	private Float Performance; //risultato della metrica in percentuale
	private Float peggiore; //performance peggiore
	private Float medio; // caso medio
	private Float migliore; //performance migliore
	//private Map<Float, List<String>> invisibleVehicleByTime; 
    //private HashMap<Float,ArrayList<Vehicle>> time_map;
	private HashMap<String,TreeMap<Float,Vehicle>> all_vehicles;
	
	/***** General info *****/
	private Integer totTime;
	private Integer totVehicles;
	private Integer totOggVehicles; //totale oggetti veicoli
	private Integer TOTpackets; //totale pacchetti ricevuti nell'intera simulazione
	private Integer TOTlogged; //totale loggati nell'intera simulazione
	private Integer TOTdiscarded; //totale scartati intera simulazione
	private Integer consedutiveDiscarded;
	private Integer NumVehiclesThatReceived; // veicoli che hanno ricevuto
	private Integer NumVehicleslogged; //numero di veicoli che sono stati loggati nell'intera simulazione
	
	
	/**** packets statistics****/
	private TreeMap<Float,ArrayList<String>> PacketsLogged_PerTime; //pacchetti loggati in ogni tempo
	private TreeMap<Float,ArrayList<String>> PacketsNeverLogged_PerTime; //pacchetti loggati in ogni tempo
	private TreeMap<Float,Integer> NumPacketsDiscarded_PerTime; //numero scartati per ogni tempo
	private TreeMap<Float,Integer> NumPacketsLogged_PerTime; //numero loggati per ogni tempo
	private HashMap<String,Integer[]> Logged_From_a_Vehicle; //loggati da un veicolo nell'intera simulazione
	
	/**** vehicle statistics ****/
	private TreeMap<Float,Integer> NumVehicleThatRecived_PerTime; //mappa del num medio di veicoli che hanno ricevuto in un tempo
	
	/******** result *********/
	private Object[] result;
	
	
	
	
	/**
	 * costruttore statistiche
	 * @param c = costanti
	 * @param all_vehicles2 
	 * @param result: 
	 */
	public Statistics(Constant c,Object[] result){
		this.c=c;
		if(result==null){
			this.totTime=new Integer (0);
			this.TOTdiscarded=new Integer (0);
			this.TOTlogged=new Integer (0);
			this.TOTpackets=new Integer (0);
			this.totOggVehicles=new Integer (0);
			this.NumVehicleslogged=new Integer (0);
			this.consedutiveDiscarded=new Integer (0);
			this.NumVehiclesThatReceived=new Integer (0);

		}else{
			// ripristino i valori del file precedente
			this.totTime=(Integer)result[0];
			this.TOTdiscarded=(Integer)result[1];
			this.TOTlogged=(Integer)result[2];
			this.TOTpackets=(Integer)result[3];
			this.totOggVehicles=(Integer)result[4];
			this.NumVehicleslogged=(Integer)result[5];
			this.consedutiveDiscarded=(Integer)result[6];
			this.NumVehiclesThatReceived=(Integer)result[7];
			this.PacketsLogged_PerTime=(TreeMap<Float,ArrayList<String>>)result[8];
			this.PacketsNeverLogged_PerTime=(TreeMap<Float,ArrayList<String>> )result[9];
			this.NumPacketsDiscarded_PerTime=(TreeMap<Float,Integer>)result[10];
			this.NumPacketsLogged_PerTime=(TreeMap<Float,Integer>)result[11];
			this.Logged_From_a_Vehicle=( HashMap<String,Integer[]>)result[12];
			this.NumVehicleThatRecived_PerTime=(TreeMap<Float,Integer>)result[13];
		}

	}
	
	/******************************** Packets statistic ***************************************/
	
	/**
	 * setting dei numeri totali di pacchetti persi loggati e scartati
	 * @param l loggati totali
	 * @param d scartati totali
	 * @param totPackets pacchetti totali
	 */
	public void TotLogged_discarded(Integer l,Integer d,Integer totPackets){ //ok
		this.setTOTlogged(l);
		this.setTOTdiscarded(d);
		this.setTOTpackets(totPackets);
	}
	
	/**
	 * Pacchetti loggati e scartati per ogni tempo
	 * @param time: tempo corrente
	 * @param logged: loggati in un tempo
	 * @param discarded: scartati in un tempo
	 * @param vlist lista: veicoli del tempo corrente
	 */
	public void Packets(Float time, Integer logged, Integer discarded,ArrayList<Vehicle> vlist){ //ok	
		ArrayList<String> PacketsNeverLogged=new ArrayList<String>();
		ArrayList<String> Packetlogged = new ArrayList<String>(); //lista pacchetti loggati
		if(this.NumPacketsDiscarded_PerTime==null)
			this.NumPacketsDiscarded_PerTime=new TreeMap<Float,Integer>();
		if(this.NumPacketsLogged_PerTime==null)
			this.NumPacketsLogged_PerTime=new TreeMap<Float,Integer>();

		if(this.PacketsLogged_PerTime==null)
			this.PacketsLogged_PerTime=new TreeMap<Float,ArrayList<String>>();
		
		this.NumPacketsLogged_PerTime.put(time, logged); //pacchetti loggati in un tempo da tutti i veicoli
		this.NumPacketsDiscarded_PerTime.put(time,discarded); 
		
		for(Vehicle v:vlist){
			PacketsNeverLogged.add(v.getPacketSent());
			Packetlogged.addAll(v.getPacketLogged()); //aggiungo tutti i pacchetti loggati dai veicoli in un tempo
		}
		PacketsNeverLogged.removeAll(Packetlogged);
		this.GetPacketsNeverLoggedPerTime().put(time, PacketsNeverLogged);
		this.PacketsLogged_PerTime.put(time, Packetlogged); 
		
	}
	
	/**
	 * @return numero medio di pacchetti loggati per ogni tempo
	 */
	public Float getAvg_PacketsLoggedPerTime(){
		Float count=new Float(0);
		for(Entry<Float,Integer> e:this.NumPacketsLogged_PerTime.entrySet()){
			count=count+e.getValue();
		}
		return count/this.NumPacketsLogged_PerTime.size();
	}
	
	/**
	 * @return numero medio di pacchetti scartati per ogni tempo
	 */
	public Float getAvg_PacketsDiscardedPerTime(){
		Float count=new Float(0);
		for(Entry<Float,Integer> e:this.NumPacketsDiscarded_PerTime.entrySet()){
			count=count+e.getValue();
		}
		return count/this.NumPacketsDiscarded_PerTime.size();
	}
	
	/**
	 * Pacchetti loggati da un veicolo nell'intera simulazione
	 */
	public void Logged_from_a_Vehicle(){
		Integer[] count=new Integer[2];
		count[0]=new Integer(0);
		count[1]=new Integer(0);
		if(this.Logged_From_a_Vehicle==null)
			this.Logged_From_a_Vehicle=new HashMap<String,Integer[]>();
		for(Entry<String,TreeMap<Float,Vehicle>> entry:this.all_vehicles.entrySet()){
			for(Entry<Float,Vehicle> entry2:entry.getValue().entrySet()){
				count[1]=count[1]+entry2.getValue().getNumPacketsR(); //somma pacchetti ricevuti
				count[0]=count[0]+entry2.getValue().getNumLoggedPacket(); //somma paccchetti loggati
			}
			this.Logged_From_a_Vehicle.put(entry.getKey(), count); //salvo entrambe le somme nella mappa per facilitare le medie
		}
	}
	
	/**
	 * 
	 * @return Num medio pacchetti loggati da un veicolo nell'intera simulazione
	 */
	public Float getAvg_LoggedFromAVehicle(){
		/*Integer countReceived=new Integer(0);
		Integer countlogged=new Integer(0);
		for(Entry<String,Integer[]> e:this.LoggedPerVehicle.entrySet()){
			countReceived+=e.getValue()[1];
			countlogged+=e.getValue()[0];
		}
		Float result=new Float(countlogged);
		result=result/this.getTotVehicles();*/
		//numero loggati / numero veicoli
		// (countReceived-countLogged)/countReceived pacchetti scartati
		//return ((float)cPerVehiclountlogged)/countReceived;
		//return new Float(countlogged/this.getTotVehicles()); num medio Pacchetti loggati dai veicoli nell'intera simulazione
		return (float)this.TOTlogged/this.totVehicles;
	}
	
	/**
	 * @return num medio Pacchetti scartati da un veicolo nell'intera simulazione
	 */
	public Float getAvg_DiscardedFromAVehicle(){
		/*Integer countReceived=new Integer(0);
		Integer countlogged=new Integer(0);
		for(Entry<String,Integer[]> e:this.LoggedPerVehicle.entrySet()){
			countReceived+=e.getValue()[1];
			countlogged+=e.getValue()[0];
		}*/
		//  media pacchetti scartati
		//return ((float)(countReceived-countlogged))/countReceived;
		//return new Float(countReceived-countlogged)/this.totVehicles;
		return (float)this.TOTdiscarded/this.totVehicles;
	}

	/**
	 * numero di volte che viene loggato lo stesso pacchetto
	 * @param vlist: lista veicoli
	 * @param time: tempo corrente
	 */
	public void NumLog_SamePacket(ArrayList<Vehicle> vlist, Float time){
		Integer count=new Integer(0);
		for(Entry<Float,ArrayList<String>> timePackets: this.PacketsLogged_PerTime.entrySet()){
			Set<String> packetsSet= new HashSet<String>();
			for(String p:timePackets.getValue()){
				packetsSet.add(p);// salvo in un set per eliminare le occorrenze
			}
			Integer size=new Integer(packetsSet.size()); 
			count = count + size;
		}
		//System.out.println("count: "+count);
		this.setNumVehicleslogged(count);

	}
	
	/**
	 * @return num medio di volte che uno stesso pacchetto viene loggato nell'intera simulazione
	 */
	public Float getAvg_LogSamePacket(){
		return (float) this.getTOTlogged()/this.getNumVehicleslogged();
	}
	
	/**
	 * dato un veicolo, qual'è il numero di pacchetti consecutivi che non sono stati loggati
	 * in tempi consecutivi
	 * da parte dello stesso veicolo
	 */
	public void ConsecutiveDiscarded(){
		Integer notlogged=new Integer(0);
		Integer tot=new Integer(0);
		Boolean flag=false;
		
		Float num=new Float(0.10);
		
		for(Entry<String,TreeMap<Float,Vehicle>> phisicVehicle:this.all_vehicles.entrySet()){
			//System.out.println("veicolo...."+phisicVehicle.getKey());
			Float timePrec= new Float(0);
			flag=false; //ogni volta che cambio veicolo
			for(Entry<Float,Vehicle> objVehicle:phisicVehicle.getValue().entrySet()){
				//se pacchetto inviato dal veicolo non è stato loggato
				Float timeCorr= new Float (objVehicle.getKey());
				Float diff=new Float(timeCorr-timePrec);
				if(timePrec==0){ //se è il primo tempo
					//System.out.println("      primo tempo...");
					timePrec=timeCorr;
					if(!this.PacketsLogged_PerTime.get(timeCorr).contains(objVehicle.getValue().getPacketSent())){ 	
					//	System.out.println("     invisibile prima volta...");
						tot=tot+notlogged;
						notlogged=0;
						flag=true;
					}
					else{ //se è stato loggato, se non è invisibile
						tot+=notlogged;
						notlogged=0;
						flag=false;
					}
				}
				else if(diff<=num){ //se il tempo corrente è consecutivo al precedente
					//System.out.println("      tempi consecutivi....tempo corr "+objVehicle.getKey()+ "   tempo prec "+timePrec+"  diff: "+diff);
					timePrec=timeCorr;
					if(!this.PacketsLogged_PerTime.get(timeCorr).contains(objVehicle.getValue().getPacketSent())){ 	
						if(flag){ //era già invisibile prima
							notlogged++;
							//System.out.println("     invisibile , era già invisibile...");
						}
						else{ //non era invisibile prima
							//System.out.println("     invisibile,  prima non lo era..");

							tot+=notlogged;
							notlogged=0;
							flag=true;
						}
					}
					else{ //se è stato loggato, se non è invisibile
						tot+=notlogged;
						notlogged=0;
						flag=false;
					}
				}
				else if(diff>num){
					//System.out.println("      tempi non consecutivi....tempo corr "+objVehicle.getKey()+ "   tempo prec "+timePrec+"  diff:"+diff);
					if(!this.PacketsLogged_PerTime.get(timeCorr).contains(objVehicle.getValue().getPacketSent())){ 	
						flag=true;
						//System.out.println("   invisibile");
					}
					else{
						flag=false;
					}
					notlogged=0;
					timePrec=timeCorr;
				}
			}
		}
	//	System.out.println("Tot consecutivi non loggati:....."+tot);
		this.setConsedutiveDiscarded(tot);
	}
	

	/**
	 * @return numero medio pacchetti non loggati per tempi consecutivi
	 */
	
	public Float getAvg_ConsecutiveDiscardedForVehicle(){
		//if(this.getConsedutiveDiscarded()==null){this.setConsedutiveDiscarded(0);}
		Float tmp=new Float(this.getConsedutiveDiscarded());
		tmp=tmp/this.totVehicles;
		return tmp;
	}
	

/******************************** Vehicles statistic ***************************************/	
	/**
	 * Veicoli che hanno ricevuto in un tempo
	 */
	public void VehicleThatReceived(ArrayList<Vehicle> vlist, Float time){
		int countRic=0; //numero di veicoli che hanno ricevuto
		for(Vehicle v:vlist){
			if(v.getNumPacketsR()!=0){//se hanno ricevuto
				countRic++;
			}
		}
		if(this.NumVehicleThatRecived_PerTime==null)
			this.NumVehicleThatRecived_PerTime=new TreeMap<Float,Integer>();
		this.NumVehicleThatRecived_PerTime.put(time,countRic);
	}
	
	/**
	 * @return Numero medio di veicoli che hanno ricevuto in un tempo
	 */
	public Float getAvg_VehicleThatReceived(){
		Integer tot=new Integer(0);
		for(Entry<Float,Integer> e:this.NumVehicleThatRecived_PerTime.entrySet()){
			tot=tot+e.getValue();
		}
		this.setNumVehiclesThatReceived(tot);
		return (float) tot/this.getTotTime();
	}
	
	
	
/****************************************Metrica*********************************************/	
	
	/**
	 * Metrica
	 */
	public void Metrica(){
		
	//	Integer[] pesi=c.getPesi();
		
		this.peggiore= new Float(0);
		Float x,y,z,k = null,j;
		Float diff_loggVeicolo =new Float(Math.abs(this.getAvg_LoggedFromAVehicle()-this.getAvg_DiscardedFromAVehicle()));
		Float diff_loggTempo =new Float(Math.abs(this.getAvg_PacketsLoggedPerTime()-this.getAvg_PacketsDiscardedPerTime()));
		//System.out.println("\ndiff:  "+diff_loggVeicolo);

		if(diff_loggVeicolo >=0 && diff_loggVeicolo <= 1){
			x=new Float(0);
		}
		else if(diff_loggVeicolo > 1 && diff_loggVeicolo <= 2){
			x=new Float(11*diff_loggVeicolo);
		}
		else{
			x=new Float(-diff_loggVeicolo);
		}
		
		if(this.getAvg_ConsecutiveDiscardedForVehicle()>=0 && this.getAvg_ConsecutiveDiscardedForVehicle()<=3){
			y=new Float(0);
		}else{
			y=new Float(30*this.getAvg_ConsecutiveDiscardedForVehicle());
		}
		
		
		if(diff_loggTempo >=0 && diff_loggTempo <= 10){
			z=new Float(0);
		}
		else if(diff_loggTempo >= this.TOTpackets/5/this.totTime && diff_loggTempo <= this.TOTpackets/this.totTime){
			z=new Float(-diff_loggTempo);
		}
		else{
			z=new Float(11*diff_loggTempo);
		}
		
		if(this.getAvg_LogSamePacket()>=1 && this.getAvg_LogSamePacket()<=2){ // se è troppo basso non va bene
			k=new Float(20*this.getAvg_LogSamePacket());
		}else {
			k=new Float(-(20*this.getAvg_LogSamePacket()));
		}
		
		j=new Float(Math.abs(this.getTotOggVehicles()/this.getTotTime()-this.getAvg_VehicleThatReceived())/3);
		
		this.migliore=new Float(11*this.getTOTpackets()/2/this.getTotVehicles()-
				11*this.getTOTpackets()/2/this.getTotVehicles()+
				20*2-
				30*0+
				11*this.getTOTpackets()/this.getTotTime()/2-
				11*this.getTOTpackets()/this.getTotTime()/2+
				3*this.getTotOggVehicles()/this.getTotTime()-
				3*this.getTotOggVehicles()/this.getTotTime());

		System.out.println("\nvalori:   x="+x+"  y="+y+"  z="+z+"  k="+k+" j="+j);
		Float corrente= new Float(x-y+z+k+j);
		/*this.peggiore=new Float(11*0-
								11*this.getTOTpackets()+
								20*0-
								30*this.getTOTpackets()+
								11*0-
								11*this.getTOTpackets()/this.getTotTime()+
								3*this.getTotOggVehicles()/this.getTotTime()-
								3*this.getTotOggVehicles()/this.getTotTime());*/

		this.medio=new Float(0 - 0 + 0 - 20*0.5 + (this.getTotOggVehicles()/this.getTotTime())*0.3);
		
		this.setPerformance(corrente);
	}
	
	
	
/******************************** setter-getter ***************************************/
	public HashMap<String,TreeMap<Float,Vehicle>> getAllVehicles(){
		return this.all_vehicles;
	}
	public void setAllVehicles(HashMap<String,TreeMap<Float,Vehicle>> a){
		this.all_vehicles=a;
		this.setTotVehicles();
	}
	
	/**
	 * @return the totVehicles
	 */
	public Integer getTotVehicles() {
		return this.totVehicles;
	}

	/**
	 * @param totVehicles the totVehicles to set
	 */
	public void setTotVehicles() {
		//this.totVehicles = this.totVehicles+this.all_vehicles.size();
		this.totVehicles = this.all_vehicles.size();
	}

	/**
	 * @return the tOTpackets
	 */
	public Integer getTOTpackets() {
		return this.TOTpackets;
	}

	/**
	 * @param tOTpackets the tOTpackets to set
	 */
	public void setTOTpackets(Integer tOTpackets) {
		this.TOTpackets = this.TOTpackets+tOTpackets;
	}

	/**
	 * @return the tOTlogged
	 */
	public Integer getTOTlogged() {
		return this.TOTlogged;
	}

	/**
	 * @param tOTlogged the tOTlogged to set
	 */
	public void setTOTlogged(Integer tOTlogged) {
		this.TOTlogged =this.TOTlogged + tOTlogged;
	}

	/**
	 * @return the tOTdiscarded
	 */
	public Integer getTOTdiscarded() {
		return this.TOTdiscarded;
	}

	/**
	 * @param tOTdiscarded the tOTdiscarded to set
	 */
	public void setTOTdiscarded(Integer tOTdiscarded) {
		this.TOTdiscarded = this.TOTdiscarded+tOTdiscarded;
	}

	/**
	 * @return the packetsLogged_PerTime
	 */
	public TreeMap<Float, Integer> getNumPacketsLogged_PerTime() {
		return this.NumPacketsLogged_PerTime;
	}

	/**
	 * @return the packetsDiscarded_PerTime
	 */
	public TreeMap<Float, Integer> getNumPacketsDiscarded_PerTime() {
		return this.NumPacketsDiscarded_PerTime;
	}


	/**
	 * @param average_LoggedFromAlmost1 the average_LoggedFromAlmost1 to set
	 */
	
	public void setTotTime(Integer tot){ //setta il numero di tempi totali
		this.totTime=this.totTime+tot;
	}
	public Integer getTotTime(){
		return this.totTime;
	}
		
	public String getTecnicaScarto(){
		return this.tecnica_scarto;
	}
	
	public void setTecnicaScarto(String t){
		this.tecnica_scarto=t;
	}
	
	/**
	 * @return the totOggVehicles
	 */
	public Integer getTotOggVehicles() {
		return this.totOggVehicles;
	}

	/**
	 * @param totOggVehicles the totOggVehicles to set
	 */
	public void setTotOggVehicles(Integer totOggVehicles) {
		this.totOggVehicles = this.totOggVehicles+totOggVehicles;
	}
	
	public Integer getNumVehiclesThatReceived() {
		return this.NumVehiclesThatReceived;
	}

	public void setNumVehiclesThatReceived(Integer numVehiclesThatReceived) {
		this.NumVehiclesThatReceived = this.NumVehiclesThatReceived + numVehiclesThatReceived;
	}

	/**
	 * @return the consedutiveDiscarded
	 */
	public Integer getConsedutiveDiscarded() {
		return this.consedutiveDiscarded;
	}

	/**
	 * @param consedutiveDiscarded the consedutiveDiscarded to set
	 */
	public void setConsedutiveDiscarded(Integer consedutiveDiscarded) {
		this.consedutiveDiscarded = consedutiveDiscarded;
	}

	

	public Float getPeggiore(){
		return this.peggiore;
	}
	public Float getMigliore(){
		return this.migliore;
	}
	public Float getMedio(){
		return this.medio;
	}
	
	/**
	 * @return the performance
	 */
	public Float getPerformance() {
		return this.Performance;
	}

	/**
	 * @param performance the performance to set
	 */
	public void setPerformance(Float performance) {
		this.Performance = performance;
	}
	


/****************************************stampa*********************************************/	
	public void Stampa(){
		
		java.util.TimeZone t=java.util.TimeZone.getTimeZone("ECT");
		java.util.Calendar oggi = java.util.Calendar.getInstance(t);
		java.util.Calendar c = java.util.Calendar.getInstance();
		
		//WriteTemp writeTemp=new WriteTemp("",this.all_vehicles,...); //scrivo il file temp
		
	/*	try{
			//nome file output: result_giorno/mese/anno_ora:minuti
			 OutputStream output = new FileOutputStream("Result_"+oggi.get(oggi.DAY_OF_MONTH)+
					 ""+oggi.get(oggi.MONTH)+1+""+oggi.get(oggi.YEAR)+"_ore"+c.get(java.util.Calendar.HOUR_OF_DAY)+
					 ""+c.get(java.util.Calendar.MINUTE)+".txt");
			 PrintStream printOut = new PrintStream(output);
			 System.setOut(printOut);
		} catch (IOException exception){ 
			System.out.println("Errore: " + exception); 
			System.exit(1); 
		}*/
		
		System.out.println("\nSimulazione del "+oggi.get(Calendar.DAY_OF_MONTH)+"/"+oggi.get(Calendar.MONTH)+1+"/"+oggi.get(Calendar.YEAR)+"  " +
				"ore "+c.get(java.util.Calendar.HOUR_OF_DAY)+":"+c.get(java.util.Calendar.MINUTE)+
				":"+c.get(java.util.Calendar.SECOND));
		
		System.out.println("Tecnica di scarto: "+this.getTecnicaScarto());

		System.out.println("\n____________Statistics___________________");
		System.out.println("\nDurata simulazione con SUMO: "+this.getTotTime()/10+"s");
		System.out.println("Totale tempi (time step): "+this.getTotTime());
		System.out.println("Totale veicoli: "+this.getTotVehicles());
		System.out.println("Totale oggetti veicolo: "+this.getTotOggVehicles());

		System.out.println("\nPacchetti totali ricevuti: "+this.getTOTpackets());      
		System.out.println("Pacchetti totali loggati: "+this.getTOTlogged());
		System.out.println("Pacchetti totali scartati: "+this.getTOTdiscarded());
		
		System.out.println("\nNum medio pacchetti loggati da un veicolo nella simulazione: "+this.getAvg_LoggedFromAVehicle());
		System.out.println("Num medio pacchetti scartati da un veicolo nella simulazione: "+this.getAvg_DiscardedFromAVehicle());
		System.out.println("Num medio pacchetti consecutivi non loggati : "+this.getAvg_ConsecutiveDiscardedForVehicle());
		System.out.println("Num medio pacchetti loggati in un tempo: "+this.getAvg_PacketsLoggedPerTime());
		System.out.println("Num medio pacchetti scartati in un tempo: "+this.getAvg_PacketsDiscardedPerTime());
		
		System.out.println("\nNum medio di volte che uno stesso pacchetto viene loggato: "+this.getAvg_LogSamePacket());//uguale
		
		System.out.println("\nNum medio veicoli in un tempo: "+(float)this.getTotOggVehicles()/this.getTotTime());
		System.out.println("Num medio veicoli che hanno ricevuto in un tempo: "+this.getAvg_VehicleThatReceived());
		
	//  System.out.println("Num medio di pacchetti loggati da almeno un veicolo: "+this.getAvg_LoggedFromAlmost1());//uguale al primo in teoria	
		}

	/**
	 * @return the packetsLogged_PerTime
	 */
	public TreeMap<Float, ArrayList<String>> getPacketsLogged_PerTime() {
		if (this.PacketsLogged_PerTime==null){
			this.PacketsLogged_PerTime=new TreeMap<Float, ArrayList<String>>();
		}
		return this.PacketsLogged_PerTime;
	}
	
	public TreeMap<Float,ArrayList<String>> GetPacketsNeverLoggedPerTime(){
		if(this.PacketsNeverLogged_PerTime==null){
			this.PacketsNeverLogged_PerTime=new TreeMap<Float,ArrayList<String>>();
		}
		return this.PacketsNeverLogged_PerTime;
	}
	
	public void provaConsecutive(){

		Integer cnt = new Integer(0);
		for (Entry<String, TreeMap<Float, Vehicle>> phisicalVehicle : this.getAllVehicles().entrySet()){ 
			TreeMap<Float, Vehicle> oggettiveicolo = phisicalVehicle.getValue(); 
			Set<Float> oggettiveicoloKeySet = oggettiveicolo.keySet();
			for(Float i:oggettiveicoloKeySet){ // per ogni tempo in cui un oggetto veicolo esiste
				// calcolo tempo successivo ESISTENTE
				Float j=new Float(i+0.10);
				while(oggettiveicoloKeySet.contains(j)){
				System.out.println("while...");
					j=new Float(j+0.10);
				}
				Float k=new Float(i+0.1);
				System.out.println("prima if tempi successivi...");
				if(j==k){
					System.out.println("tempi successivi...");
					Vehicle vi=oggettiveicolo.get(i);
					Vehicle vj=oggettiveicolo.get(j);
					if(this.getPacketsLogged_PerTime().get(i).contains(vi.getPacketSent())&&this.getPacketsLogged_PerTime().get(j).contains(vj.getPacketSent())){
						cnt++;
						System.out.println("aumento count.......");
					}
				}
			}
		}
		this.setConsedutiveDiscarded(cnt);
		System.out.println("prova.............:" +cnt);
	}
	
	public void stampaPacchettiLoggati(){
		int count=0;
		int count2=0;
		for(Entry<Float,Integer> e:this.NumPacketsLogged_PerTime.entrySet()){
			count2=count2+e.getValue();
		}
		for(Entry<Float,ArrayList<String>> time:this.PacketsLogged_PerTime.entrySet()){
			System.out.println("\n\n\ntime: "+time.getKey());
			count=count+time.getValue().size();
			for(String p:time.getValue()){
				//count++;
				System.out.println("   veicolo: "+p);
			}
		}
		//System.out.println("count: "+count+" count2: "+count2);
	}

	/**
	 * @return the numVehicleslogged
	 */
	public Integer getNumVehicleslogged() {
		return this.NumVehicleslogged;
	}

	/**
	 * @param numVehicleslogged the numVehicleslogged to set
	 */
	public void setNumVehicleslogged(Integer numVehicleslogged) {
		this.NumVehicleslogged = this.NumVehicleslogged + numVehicleslogged;
	}

	/*
	 * dato un veicolo, qual'è il numero di pacchetti consecutivi che non sono stati loggati
	 * da parte dello stesso veicolo
	 
	public void ConsecutiveDiscarded2(){
		String vehicle_corr; //pacchetto corrente
		Boolean trovato=false;
		int notlogged=0;
		int tempMax=0;
		for(Entry<String,TreeMap<Float,Vehicle>> entry1:this.all_vehicles.entrySet()){ // per ogni veicolo (NON OGGETTO)
			HashMap<Packet,Integer> consecutive_packets = new HashMap<Packet,Integer>();
			for(Entry<Float,Vehicle> entry2:entry1.getValue().entrySet()){ // per ogni tempo ricavo l'oggetto veicolo
				
				for(Packet pcorr:entry2.getValue().getPacketDiscarded()){ //scorro pacchetti scartati
					
					
					vehicle_corr=pcorr.getV().getId(); //salvo il veicolo che lo ha emesso 
					
					for(Entry<String,TreeMap<Float,Vehicle>> entry11:this.all_vehicles.entrySet()){ //scorro di nuovo allvehicle
						if(entry11.getKey().equals(entry1.getKey())){//controllo su stringa
							for(Entry<Float,Vehicle> entry22:entry1.getValue().entrySet()){
								if(entry22.getKey()!=entry2.getKey()){//controllo sul tempo
									for(Packet p:entry22.getValue().getPacketDiscarded()){
										if(p.getV().getId().equals(vehicle_corr)){
											//se la lista dei pacchetti scartati contiene il pacchetto corrente che sto confrontando
											//allora aumento il contatore dei non loggati, perchè vuol dire
											//che il pacchetto di prima non è stato loggato nemmeno nel tempo successivo
											notlogged++;
											if(notlogged>tempMax)
												tempMax=notlogged; //salvo il massimo numero di volte consecutive
											trovato = true;
											break;
										}
									}
									if(trovato==false){ // se il pacchetto non c'è tra quelli scartati allora risetto il contatore
										notlogged=0;
									}
								}
							}//fine tempi
							//devo salvare il pacchetto corrisponente al veicolo che non è stato loggato e il suo contatore
							if(tempMax>this.c.getSoglia()){
								consecutive_packets.put(pcorr, tempMax);
							}
							tempMax=0; //reset tempMax
						}//fine if conrollo stringa
					}
				}
			}
			if(this.ConsecutiveDiscarded==null)
				this.ConsecutiveDiscarded=new HashMap<String,HashMap<Packet,Integer>>();
			if(consecutive_packets!=null)
				this.ConsecutiveDiscarded.put(entry1.getKey(), consecutive_packets);
		}
		
		
	}*/
	
	/*
	 *  numero Pacchetti loggati da almeno un veicolo in un dato tempo
	 *  coincide con il numero dei pacchetti loggati
	 *  //FIXME in pacch. loggati da un tot di veicoli(tipo il 10%)
	 * vlist=lista dei veicoli in un tempo 
	 
	public void loggedFromAlmost1(ArrayList<Vehicle> vlist, Float time){
		Integer count=new Integer(0);
		
		if(this.LoggedFromAlmost1==null){
			
			this.LoggedFromAlmost1=new TreeMap<Float,Integer[]>();
		}
		for(Vehicle v:vlist){
			//this.averageLoggedPacket(v, time);
			for(Vehicle vdistance:v.getV100m()){ //scorro veicoli a 100m per ogni veicolo
				//for(Packet plog:vdistance.getPacketLogged()){ //scorro pacchetti loggati
				if(vdistance.getPacketLogged().contains(v.getPacketSent())){
					count++;
				}
				//}
			}
		}
		Integer[] vett=new Integer[2];
		vett[0]=count;//numero Pacchetti loggati da almeno un veicolo in un dato tempo
		vett[1]=vlist.size(); //totale pacchetti inviati = a totale veicoli in un tempo
		this.LoggedFromAlmost1.put(time,vett); //per ogni tempo inserisco il conteggio	
	}*/
	
	/*
	 * @return ritorna media, pacchetti loggati / pacchetti ricevuti nell'intera simulazione
	 
	public Float getAvg_LoggedFromAlmost1() {
		Integer countPacketLogged=new Integer(0);
		Integer countTotPacket=new Integer(0);
		for(Entry<Float,Integer[]> entry:this.LoggedFromAlmost1.entrySet()){
			Integer[] a= entry.getValue();
			countPacketLogged+=a[0];
			countTotPacket+=a[1];
		}
		//this.Average_LoggedFromAlmost1=  (float)(countPacketLogged)/countTotPacket;
		//this.Average_LoggedFromAlmost1=(float) (count/this.LoggedFromAlmost1.size());
		return (float)(countPacketLogged)/this.totVehicles;
	}*/

	public void svuota(){
	
		if(this.PacketsLogged_PerTime.size()!=0){
			/*for(Entry<Float,ArrayList<Packet>> time:this.PacketsLogged_PerTime.entrySet()){
				for(Packet p:time.getValue()){
					p.svuota();
				}
			}*/
			this.PacketsLogged_PerTime.clear();
		}
		
		if(this.PacketsNeverLogged_PerTime.size()!=0){
		/*	for(Entry<Float,ArrayList<Packet>> time:this.PacketsNeverLogged_PerTime.entrySet()){
				for(Packet p:time.getValue()){
					p.svuota();
				}
			}*/
			this.PacketsNeverLogged_PerTime.clear();
		}
	}

	public HashMap<String,Integer[]> GetLogged_From_a_Vehicle(){
		if(this.Logged_From_a_Vehicle==null){
			this.Logged_From_a_Vehicle=new HashMap<String, Integer[]>();
		}
		return this.Logged_From_a_Vehicle;
	}
	
	/**
	 * 
	 * @return a object array that contain the previous computation of value needed for evaluating the Discarding strategy
	 * the value are stored in that way
	 * 	result[0]=TotTime;
	*	result[1]=TOTdiscarded;
	*	result[2]=TOTlogged
	*	result[3]=TOTpackets
	*	result[4]=TotOggVehicle
	*	result[5]=NumVehicleslogged
	*	result[6]=ConsedutiveDiscarded
	*	result[7]=NumVehiclesThatReceived;
	*	result[8]=PacketsLogged_PerTime;
	*	result[9]=PacketsNeverLoggedPerTime;
	*	result[10]=NumPacketsDiscarded_PerTime;
	*	result[11]=NumPacketsLogged_PerTime;
	*	result[12]=Logged_From_a_Vehicle;
	*	result[13]=NumVehiclesThatReceivedPerTime;
	 * 
	 */
	public Object[] getResult() {//TODO leggi
		if(this.result==null){
			this.result=new Object[15];
		}
		result[0]=this.getTotTime();
		result[1]=this.getTOTdiscarded();
		result[2]=this.getTOTlogged();
		result[3]=this.getTOTpackets();
		result[4]=this.getTotOggVehicles();
		result[5]=this.getNumVehicleslogged();
		result[6]=this.getConsedutiveDiscarded();
		result[7]=this.getNumVehiclesThatReceived();
		result[8]=this.getPacketsLogged_PerTime();
		result[9]=this.GetPacketsNeverLoggedPerTime();
		result[10]=this.getNumPacketsDiscarded_PerTime();
		result[11]=this.getNumPacketsLogged_PerTime();
		result[12]=this.GetLogged_From_a_Vehicle();
		result[13]=this.getNumVehiclesThatReceivedPerTime();
		return result;
	}
	








	
	
	
	
	
	
	
	
	
	

	private TreeMap<Float,Integer> getNumVehiclesThatReceivedPerTime() {
		if(this.NumVehicleThatRecived_PerTime==null){
			this.NumVehicleThatRecived_PerTime=new  TreeMap<Float,Integer>();
		}
		return this.NumVehicleThatRecived_PerTime;
	}
	public void setResult(Object[] result) {
		this.result = result;
	}
}


