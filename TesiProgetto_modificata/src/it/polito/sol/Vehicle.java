package it.polito.sol;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.TreeMap;


public class Vehicle implements Comparable<Vehicle> {

	private String id;
	//private String lane;
	//private Float x;
	//private Float y;
	private Float lat;
	private Float lon;
	private Float speed; //FIME NON SERVE

	
	//TODO TOGLIERE (LASCI I GETTER E FAI RITORNARE MAPPA.SIZE COSI NN DEVI CAMBIARE CODICE)
	//private Float pos;  
	private Integer numPacketsR; // numero pacchetti ricevuti per ogni veicolo in un tempo
	private Integer interval; // per vedere in che momento è stato inviato il pacchetto
	private Float time; //tempo totale
	private Float convertedTime; //intervallino di tempo convertito
	private Integer numLoggedPacket; //pacchetti loggati in un tempo
	private Integer numDiscardedPacket;
	


	private TreeMap<Float, ArrayList<String>> packets_received; // mappa dei pacchetti ricevuti in ogni tempo-float=intervallino
	private TreeMap<Float, ArrayList<Vehicle>> vehicles_100m; // veicoli a 100m con distanze  
	//todo  VEHICLE_100M(QUESTA SERVE SL 1 VOLTA NO? QUINDI PUOI NN SALVARLA DIRETTAMENTE)	 COME SOPRA....					
	private ArrayList<Vehicle> v100m; // solo veicoli a 100m // TODO questa nn serve proprio ke � gia dentro vehicles_100m
	private ArrayList<String> packets_logged; // lista dei pacchetti loggati (riempita dopo aver
	// applicato una strategia) Float=tempo generale
	//private ArrayList<Packet> packets_discarded; // lista pacchetti scartati
	

	public Vehicle(String id, Float lat, Float lon, Float speed, Float time) {
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.speed = speed;
		this.setTime(time);
		this.v100m = new ArrayList<Vehicle>(); 
		this.vehicles_100m = new TreeMap<Float, ArrayList<Vehicle>>();
		this.packets_logged = new ArrayList<String>();
		this.packets_received=new TreeMap<Float, ArrayList<String>>();
	}

	// getter

	public String getId() {
		return this.id;
	}

	public Float getLat() {
		return this.lat;
	}

	public Float getLon() {
		return this.lon;
	}

	public Float getSpeed() {
		return this.speed;
	}

	public Integer getInterval() {
		return this.interval;
	}

	public void setInterval(Integer f, Float t) {
		this.interval = f;
		//this.intervalTime=new Float((t+ ((float) f / 100)));
	}

	@Override
	public int compareTo(Vehicle v) {
		int res = this.id.compareTo(v.getId());
		return res; // ritorna 0 se sono uguali
	}

	/**
	 * this function add time to the list of time when the vehicle send packet
	 * @param i is the interval
	 * @param time is the time_step
	 */
	

	/**
	 * @return the packetSent
	 */
	public String getPacketSent() {
		return this.id;
	}

	/**
	 * @return the packets_received
	 */
	public TreeMap<Float, ArrayList<String>> getPackets_received() {
		return this.packets_received;
	}

	/**
	 * @param packets_received
	 *            the packets_received to set
	 */
	public void setPackets_received(Float time, String p) {
		this.numPacketsR=this.getNumPacketsR()+1;
		if (this.packets_received.containsKey(time)) { // se ho già quella chiave aggiungo solo la lista
			ArrayList<String> plist = this.packets_received.get(time); // lista pacchetti rcevuti a quel tempo quel veicolo				
			plist.add(p);

			//this.numPacketsR++;
			// System.out.println("pacchetti ric....."+this.packets_received);
		} else {
			ArrayList<String> plist = new ArrayList<String>(); // se mappa non ha questa chiave
			plist.add(p);
			this.packets_received.put(time, plist);
			//this.numPacketsR++;
		}
	}

	public void addDistances(Float distance, Vehicle v,Float dmin, Float dmax) { 
		if (this.vehicles_100m.containsKey(distance)) { // se la mappa contiene la distanza
			ArrayList<Vehicle> Vlist = this.vehicles_100m.get(distance); // ritorna array dei veicoli per quella dist
			if (!Vlist.contains(v)) { // se il veicolo da inserire non c'è lo aggiungo
				Vlist.add(v);
				this.addV100m(v);
			}
		} else { // se la mappa non contiene la distanza creo l'array e aggiungo alla mappa distanze
			ArrayList<Vehicle> Vlist = new ArrayList<Vehicle>();
			Vlist.add(v);
			this.vehicles_100m.put(distance, Vlist);
			this.addV100m(v);
		}
	}

	/**
	 * getter e setter di vehicle100m ritorna una mappa di veicoli entro una
	 * certa distanza, contiene la distanza effettiva di ogni veicolo
	 */
	public TreeMap<Float, ArrayList<Vehicle>> getVehicleIndistance() {
		return this.vehicles_100m;
	}

	/*
	 * getter e setter di V100m, lista di soli veicoli a 100m
	 */
	public void addV100m(Vehicle v) {
		this.v100m.add(v);
	}

	public ArrayList<Vehicle> getV100m() {
		if(this.v100m==null){
			this.v100m=new ArrayList<Vehicle>();
		}
		return this.v100m;
	}

	/**
	 * Float = tempo generale, non intervallini
	 * getter e setter di packetlogged
	 */
	public void setPacketLogged(ArrayList<String> pl) {
		this.packets_logged=pl;
	}

	public ArrayList<String> getPacketLogged() {
		return this.packets_logged;
	}

	public void setPacketDiscarded(ArrayList<String> pl) {
		this.packets_logged=pl;
	}

	public ArrayList<String> getPacketDiscarded() {
		return this.packets_logged;
	}



	/**
	 * @return the numPacketsR
	 */
	public Integer getNumPacketsR() {
		if(this.numPacketsR==null){this.numPacketsR=new Integer(0);}
		return this.numPacketsR;
	}


	public Float convert_time(Float time,Integer i){
		Float t=new Float((time+((float)i/100)));
		t=(float)Math.round(t*100);
		t=t/100;
		this.convertedTime = t;
		return t;
	}

	/**
	 * @return the time
	 */
	public Float getTime() {
		return this.time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Float time) {
		this.time = time;
	}

	/**
	 * @return the convertedTime
	 */
	public Float getConvertedTime() {
		return this.convertedTime;
	}

	/**
	 * @return the numLoggedPacket
	 */
	public Integer getNumLoggedPacket() {
		if(this.numLoggedPacket==null){this.numLoggedPacket=new Integer(0);}
		return this.numLoggedPacket;
	}
	/**
	 * @param numDiscardedPacket the numDiscardedPacket to set
	 */
	public void setNumLoggedPacket(Integer numLoggedPacket) {
		this.numLoggedPacket = numLoggedPacket;
	}
	/**
	 * @return the numDiscardedPacket
	 */
	public Integer getNumDiscardedPacket() {
		if(this.numDiscardedPacket==null){this.numDiscardedPacket=new Integer(0);}
		return numDiscardedPacket;
	}

	/**
	 * @param numDiscardedPacket the numDiscardedPacket to set
	 */
	public void setNumDiscardedPacket(Integer numDiscardedPacket) {
		this.numDiscardedPacket = numDiscardedPacket;
	}


	public void svuota(){
		if(this.packets_received!=null &&this.packets_received.size()!=0){
			for(Entry<Float, ArrayList<String>> e:this.packets_received.entrySet()){
			/*	for(Packet p:e.getValue()){
					if(p!=null){
						//p.svuota();
						p=null;
					}
					
				}*/
				e.getValue().clear();
			}
			this.packets_received.clear();
		}
		

		if(this.vehicles_100m!=null&& !this.vehicles_100m.isEmpty()){
			for( ArrayList<Vehicle> e :this.vehicles_100m.values() ){
				if(e!=null&& !e.isEmpty()){
					} e.clear();
				}
			this.vehicles_100m.clear();
		}


		if(this.packets_logged!=null && this.packets_logged.size()!=0){
			/*for(Packet p:this.packets_logged){
					if(p!=null){
						p.svuota();
						p=null;
					}
				}*/
				this.packets_logged.clear();
			}

	}
}
	