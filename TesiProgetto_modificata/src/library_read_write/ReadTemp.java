package library_read_write;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;


import it.polito.temp.SendIntervalType;
import it.polito.temp.StatisticType;
import it.polito.temp.TempType;

public class ReadTemp {
 private Integer[] tempStat;
 private Map<String,Integer> SendInterval;
 private Object[] result;
 
 
 /**
  * 
  * @param tmpXmlPath url del file temporaneo 
  * 
  * read the temp file and parse it
  * call getResult() to obtain the result
  */
 public ReadTemp(String tmpXmlPath){
	 try {
		 
		 // faccio unMarshal
		JAXBContext jc = JAXBContext.newInstance("it.polito.temp");
		Unmarshaller u = jc.createUnmarshaller();
		SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
		File file =new File(tmpXmlPath);
		String pathXmlSchema=file.getAbsolutePath();
		pathXmlSchema=pathXmlSchema.replace(file.getName(), "temp.xsd");
		Schema schema = sf.newSchema(new File(pathXmlSchema));
		u.setSchema(schema);
		u.setEventHandler(
				new ValidationEventHandler() {
					public boolean handleEvent(ValidationEvent ve) {
						// ignore warnings
						if (ve.getSeverity() == ValidationEvent.WARNING) {
							ValidationEventLocator vel = ve.getLocator();
							System.out.println("Warning");
							System.out.println("Line:Col[" + vel.getLineNumber() +
									":" + vel.getColumnNumber() +
									"]:" + ve.getMessage());
						} else {
							// block if fatal error or error
							ValidationEventLocator vel = ve.getLocator();
							System.out.println("ERROR during schema validation");
							System.out.println("Line:Col[" + vel.getLineNumber() +
									":" + vel.getColumnNumber() +
									"]:" + ve.getMessage());
							return false;
						}
						return true;
					}
				}	
				);
		
		@SuppressWarnings("unchecked")
		JAXBElement<TempType> tmpRootJAXB = (JAXBElement<TempType>) u.unmarshal(file); 
			TempType root=tmpRootJAXB.getValue();
			
			for(SendIntervalType elem: root.getSendInterval()){
				this.getSendInterval().put(elem.getVehicleId(), elem.getInterval());
			}
			StatisticType stat=root.getStatistic();
			Integer[] statResult = this.getTempStat();
			statResult[0]=stat.getNumberOfVehicle();
			statResult[1]=stat.getNumberOfTime();
			statResult[2]=stat.getNumberReceivedPackets();
			statResult[3]=stat.getNumberLoggedPackets();
			statResult[4]=stat.getNumberDiscardedPackets();
			statResult[5]=stat.getNumberConsecutiveDiscarded();
			statResult[6]=stat.getNumberVehicleThatReceive();
			
			// c'� da "parsare la mappa per ultimo tempo x vedere i consecutivi
			Object lastTime=new Object();
			
			
		
			// i ritorni sn gia fatti
			this.result=new Object[3];
			this.result[0]=this.getSendInterval();
			this.result[1]=statResult;
			this.result[2]=lastTime;
			
	 }
		 catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	       
	 catch (JAXBException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
 }


public Integer[] getTempStat() {
	if(this.tempStat==null){
		this.tempStat=new Integer[10];
	}
	return this.tempStat;
}


public void setTempStat(Integer[] tempStat) {
	this.tempStat = tempStat;
}




public Map<String,Integer> getSendInterval() {
	if(this.SendInterval==null){
		this.SendInterval=new HashMap<String, Integer>();
	}
	return this.SendInterval;
}


public void setSendInterval(Map<String,Integer> sendInterval) {
	SendInterval = sendInterval;
}


/**
 * 
 * @return the information parsed from the temp file 
 *   vett[0]: send interval map<vehicleId,Interval>
 *   vett[1]: Integer[] containg the temp valuo of counted statistic in the same order of their definition in the xsd file  
 *   vett[2]: info per tenere conto di ultimo tempo per veicoli invisibili 
 */
public Object[] getResult() {
	return this.result;
}


 
}