package library_read_write;


import it.polito.sol.Vehicle;
import it.polito.temp.ObjectFactory;
import it.polito.temp.SendIntervalType;
import it.polito.temp.StatisticType;
import it.polito.temp.TempType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.*;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;


public class WriteTemp {
	private ObjectFactory of;
	
	public WriteTemp(String XmlTempPath, Map<String,TreeMap<Float,Vehicle>> allvehicle,Integer[] statTemp,Object last) throws Exception 	{
		TempType temp=this.getOf().createTempType();
		// riempio intervallini
		for (Entry<String, TreeMap<Float, Vehicle>> vehicleL:allvehicle.entrySet()){
			
			for(Entry<Float, Vehicle>e : vehicleL.getValue().entrySet() ){
				SendIntervalType sendinterval = this.getOf().createSendIntervalType();
				sendinterval.setInterval(e.getValue().getInterval());
				sendinterval.setVehicleId(vehicleL.getKey());
				temp.getSendInterval().add(sendinterval);
				break;
			}
			
		}
		
		// riempio totali statistiche
		StatisticType stattemp = this.getOf().createStatisticType();
		stattemp.setNumberOfVehicle(statTemp[0]);
		stattemp.setNumberOfTime(statTemp[1]);
		stattemp.setNumberReceivedPackets(statTemp[2]);
		stattemp.setNumberLoggedPackets(statTemp[3]);
		stattemp.setNumberDiscardedPackets(statTemp[4]);
		stattemp.setNumberConsecutiveDiscarded(statTemp[5]);
		stattemp.setNumberVehicleThatReceive(statTemp[6]);
		
		
		temp.setStatistic(stattemp);
	
		// manca ultima parte per concecutivo
		
		
		//scrivo
		Marshaller m;
		try {
			JAXBContext jContext = JAXBContext.newInstance("it.polito.dp2.temp");
			m = jContext.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.example.org/temp/ temp.xsd");
			m.marshal(temp, new FileOutputStream(new File(XmlTempPath)));
		} catch (JAXBException e) {
			throw new Exception(e.getMessage(),e);
		} catch (FileNotFoundException e) {
			throw new Exception(e.getMessage(),e);
		} catch (Exception e){
			throw new Exception(e.getMessage(),e);
		}
		
	
	}






public ObjectFactory getOf() {
	if(of==null){this.of= new ObjectFactory();}
	return this.of;
}


public void setOf(ObjectFactory of) {
	this.of = of;
}
}